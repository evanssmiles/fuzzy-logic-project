#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip> //header untuk setprecission
#include <conio.h>
#include <algorithm> //std::max dan min
#include <windows.h> //untuk menggunakan gotoxy
#include <unistd.h> // untuk sleep() di animasi loading

using namespace std;

//MENEMPATKAN CONSOLE WINDOW DI TENGAH LAYAR  			sumber : http://www.cplusplus.com/forum/beginner/127593/
//Mencari tahu Resolusi Screen Si User
int Width = GetSystemMetrics(SM_CXSCREEN);
int Height = GetSystemMetrics(SM_CYSCREEN);

//Penugasan variabel untuk parameter MoveWindows
int WindowWidth = 800;		//Gunakan parameter untuk menetapkan Lebar console windows (MoveWindows int nWidth)
int WindowHeight = 500;		//Gunakan parameter untuk menetapkan Tinggi console windows (MoveWindows int nHeight)
int NewWidth = (Width - WindowWidth) / 2;		//Gunakan parameter agar console window ke tengah garis secara horizontal (MoveWindows int x)
int NewHeight = ((Height - WindowHeight) / 2) - 20;		//Gunakan parameter agar console window ke tengah garis secara vertikal (MoveWindows int y)

//Getting the console window handle
HWND hWnd = GetConsoleWindow();

//Fungsi untuk bisa menggunakan gotoxy
void gotoxy(int x, int y)   
{
     COORD coord;
     coord.X = x;
     coord.Y = y;
     SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

//tipe data untuk Luas Ruangan
float LuasRuangan, Kecil, Sedang_Ruang, Besar, NA_LuasRuangan_Kecil, NA_LuasRuangan_SedangRuang, NA_LuasRuangan_Besar; 

//tipe data untuk jumlah orang
float JumlahOrang, Sedikit, Sedang, Banyak, NA_JumlahOrang_Sedikit, NA_JumlahOrang_Sedang, NA_JumlahOrang_Banyak;

//tipe data untuk alpa predikat Dan nilai Z
float alpa1, alpa2, alpa3, alpa4, alpa5, alpa6, alpa7, alpa8, alpa9;
float Z1, Z2, Z3, Z4, Z5, Z6, Z7, Z8, Z9, Z, tempZ1, tempZ2;
int Menu;
string Suhu;

int main(int argc, char *argv[]) {
	//Gunakan Fungsi untuk menempatkan console window di tengah
	MoveWindow(hWnd, NewWidth, NewHeight, WindowWidth, WindowHeight, TRUE);
	
	do
	{
		gotoxy(20,0);cout<<"============================================";      //header tabel
		gotoxy(20,1);cout<<"|     Pengaturan AC dengan Fuzzy Logic     |";
		gotoxy(20,2);cout<<"============================================";
	    gotoxy(20,3);cout<<" 1. Mulai ";
	    gotoxy(20,4);cout<<" 2. Keluar ";
	    
	    gotoxy(20,7);cout<<"Masukkan Pilihan : ";
		cin>>Menu;
	    
	    switch(Menu)
		{
				case 1 :system("cls");
						//animasi untuk loading
						gotoxy(30,15);
						std::cout << "Tunggu Sebentar";
    					for (int i=0; i<1; i++)
    					{
				        sleep(1);
				        std::cout << "." << std::flush;
				        sleep(1);
				        std::cout << "." << std::flush;
				        sleep(1);
				        std::cout << "." << std::flush;
				        sleep(1);
				        std::cout<< "\b\b\b   \b\b\b" << std::flush;
				        
				        system("cls"); //hapus layar loading
    					
						gotoxy(20,0);cout<<"============================================";      //header tabel
						gotoxy(20,1);cout<<"|     Pengaturan AC dengan Fuzzy Logic     |";
						gotoxy(20,2);cout<<"============================================";
						
						gotoxy(35,4);cout<<"Note : Luas ruangan range antara 20-60"; //untuk note range inputan Luas Ruangan
						gotoxy(1,5);cout<<"Masukkan Luas Ruangan : ";
						cin>>LuasRuangan;
						
						
						//cari nilai keanggotaan Luas Ruangan
						if(LuasRuangan>=20 and LuasRuangan<35)
					 	{
					 	  NA_LuasRuangan_Kecil = (35-LuasRuangan)/(float)(15); //hitung nilai keanggotaan kecil luas ruangan
					 	  NA_LuasRuangan_SedangRuang = (LuasRuangan-20)/(float)(15); //hitung nilai keanggotaan sedang luas ruangan
					 	  //output Nilai Keanggotaan
						  gotoxy(1,6);cout<<setprecision(2)<<"LrKECIL \t: "<<NA_LuasRuangan_Kecil;
						  gotoxy(1,7);cout<<setprecision(2)<<"LrSEDANG \t: "<<NA_LuasRuangan_SedangRuang;
					 	} 
						 else if(LuasRuangan>=35 and LuasRuangan<=45)
					 		{
							  NA_LuasRuangan_SedangRuang = 1; //hitung nilai keanggotaan sedang luas ruangan
					 	  	  NA_LuasRuangan_Besar = 0; //hitung nilai keanggotaan besar luas ruangan
					 	  	  //output Nilai Keanggotaan
						  	  gotoxy(1,6);cout<<setprecision(2)<<"LrSEDANG \t: "<<NA_LuasRuangan_SedangRuang;
						   	  gotoxy(1,7);cout<<setprecision(2)<<"LrBESAR \t: "<<NA_LuasRuangan_Besar;
					 		}
							else if(LuasRuangan>45 and LuasRuangan<=60)
					 			{
								  NA_LuasRuangan_SedangRuang = (60-LuasRuangan)/(float)(15); //hitung nilai keanggotaan sedang luas ruangan
							 	  NA_LuasRuangan_Besar = (LuasRuangan-45)/(float)(15); //hitung nilai keanggotaan besar luas ruangan
							 	  //output Nilai Keanggotaan
								  gotoxy(1,6);cout<<setprecision(2)<<"LrSEDANG \t: "<<NA_LuasRuangan_SedangRuang;
								  gotoxy(1,7);cout<<setprecision(2)<<"LrBESAR \t: "<<NA_LuasRuangan_Besar;
								 }
								else
									 {   //kondisi ketika input tidak memenuhi batas Luas Ruangan
									  NA_LuasRuangan_Kecil = 0; //hitung nilai keanggotaan kecil luas ruangan
								 	  NA_LuasRuangan_SedangRuang = 0; //hitung nilai keanggotaan sedang luas ruangan
								 	  NA_LuasRuangan_Besar = 0; //hitung nilai keanggotaan kecil luas ruangan
								 	  //output Nilai Keanggotaan
									  gotoxy(1,6);cout<<setprecision(2)<<"LrKECIL \t: "<<NA_LuasRuangan_Kecil;
									  gotoxy(1,7);cout<<setprecision(2)<<"LrSEDANG \t: "<<NA_LuasRuangan_SedangRuang;
									  gotoxy(1,8);cout<<setprecision(2)<<"LrBESAR \t: "<<NA_LuasRuangan_Besar;				  
									 }
									 
						gotoxy(35,9);cout<<"Note : Luas ruangan range antara 3-10"; //untuk note range inputan
						gotoxy(1,10);cout<<"Masukkan Jumlah Orang : ";
						cin>>JumlahOrang;
						
						//cari nilai keanggotaan Jumlah Orang
						if(JumlahOrang>=3 and JumlahOrang<5)
					 	{
					 	  NA_JumlahOrang_Sedikit = (5-JumlahOrang)/(float)(2); //hitung nilai keanggotaan kecil Jumlah Orang
					 	  NA_JumlahOrang_Sedang = (JumlahOrang-3)/(float)(2); //hitung nilai keanggotaan sedang Jumlah Orang
					 	  //output Nilai Keanggotaan
						  gotoxy(1,11);cout<<setprecision(2)<<"JoSEDIKIT \t: "<<NA_JumlahOrang_Sedikit;
						  gotoxy(1,12);cout<<setprecision(2)<<"JoSEDANG \t: "<<NA_JumlahOrang_Sedang;
					 	} 
						 else if(JumlahOrang>=5 and JumlahOrang<=7)
					 		{
							  NA_JumlahOrang_Sedang= 1; //hitung nilai keanggotaan sedang Jumlah Orang
					 	  	  NA_JumlahOrang_Banyak = 0; //hitung nilai keanggotaan besar Jumlah Orang
					 	  	  //output Nilai Keanggotaan
						  	  gotoxy(1,11);cout<<setprecision(2)<<"JoSEDANG \t: "<<NA_JumlahOrang_Sedang;
						   	  gotoxy(1,12);cout<<setprecision(2)<<"JoBANYAK \t: "<<NA_JumlahOrang_Banyak;
					 		}
							else if(JumlahOrang>7 and JumlahOrang<=10)
					 			{
								  NA_JumlahOrang_Sedang = (10-JumlahOrang)/(float)(3); //hitung nilai keanggotaan sedang Jumlah Orang
							 	  NA_JumlahOrang_Banyak = (JumlahOrang-7)/(float)(3); //hitung nilai keanggotaan besar Jumlah Orang
							 	  //output Nilai Keanggotaan 
								  gotoxy(1,11);cout<<setprecision(2)<<"JoSEDANG \t: "<<NA_JumlahOrang_Sedang;
								  gotoxy(1,12);cout<<setprecision(2)<<"JoBANYAK \t: "<<NA_JumlahOrang_Banyak;
								 }
								else
									 {   //kondisi ketika input tidak memenuhi batas Luas Ruangan
									  NA_JumlahOrang_Sedikit = 0; //hitung nilai keanggotaan kecil Jumlah Orang
								 	  NA_JumlahOrang_Sedang = 0; //hitung nilai keanggotaan sedang Jumlah Orang
								 	  NA_JumlahOrang_Banyak = 0; //hitung nilai keanggotaan kecil Jumlah Orang
								 	  //output Nilai Keanggotaan
									  gotoxy(1,11);cout<<setprecision(2)<<"JoSEDIKIT \t: "<<NA_JumlahOrang_Sedikit;
									  gotoxy(1,12);cout<<setprecision(2)<<"JoSEDANG \t: "<<NA_JumlahOrang_Sedang;
									  gotoxy(1,13);cout<<setprecision(2)<<"JoBANYAK \t: "<<NA_JumlahOrang_Banyak;
									 }		 
						getch(); //tahan layar
						
						//Mencari ALpa Predikat Dan Zn Dari Aturan Yang Dibuat
						//1. Jika Luas Ruangan KECIL dan Jumlah Orang SEDIKIT Maka DINGIN
						alpa1 = min(NA_LuasRuangan_Kecil,NA_JumlahOrang_Sedikit);
						gotoxy(1,15);cout<<"Alpa Predikat1 : "<<alpa1;
						Z1 = 20-(4*alpa1); //himpunan suhu optimal DINGIN
						gotoxy(1,16);cout<<"Nilai Z1 \t: "<<Z1;
						
						//2. Jika Luas Ruangan KECIL dan Jumlah Orang SEDANG Maka DINGIN
						alpa2 = min(NA_LuasRuangan_Kecil,NA_JumlahOrang_Sedang);
						gotoxy(1,18);cout<<"Alpa Predikat2 : "<<alpa2<<endl;
						Z2 = 20-(4*alpa2); //himpunan suhu optimal DINGIN
						gotoxy(1,19);cout<<"Nilai Z2 \t: "<<Z2;
						
						//3. Jika Luas Ruangan KECIL dan Jumlah Orang BANYAK Maka SEJUK
						alpa3 = min(NA_LuasRuangan_Kecil,NA_JumlahOrang_Banyak);
						gotoxy(1,21);cout<<"Alpa Predikat3 : "<<alpa3<<endl;
						Z3 = 24-(2*alpa3); //himpunan suhu optimal SEJUK
						gotoxy(1,22);cout<<"Nilai Z3 \t: "<<Z3;
						
						//4. Jika Luas Ruangan SEDANG dan Jumlah Orang SEDIKIT Maka SEDANG
						alpa4 = min(NA_LuasRuangan_SedangRuang,NA_JumlahOrang_Sedikit);
						gotoxy(1,24);cout<<"Alpa Predikat4 : "<<alpa4<<endl;
						Z4 = 16-(4*alpa4); //himpunan suhu optimal SEDANG
						gotoxy(1,25);cout<<"Nilai Z4 \t: "<<Z4;
						
						//5. Jika Luas Ruangan SEDANG dan Jumlah Orang SEDANG Maka SEDANG
						alpa5 = min(NA_LuasRuangan_SedangRuang,NA_JumlahOrang_Sedang);
						gotoxy(1,27);cout<<"Alpa Predikat5 : "<<alpa5<<endl;
						Z5 = 16-(4*alpa5); //himpunan suhu optimal SEDANG
						gotoxy(1,28);cout<<"Nilai Z5 \t: "<<Z5;
						
						//6. Jika Luas Ruangan SEDANG dan Jumlah Orang BANYAK Maka SEJUK
						alpa6 = min(NA_LuasRuangan_SedangRuang,NA_JumlahOrang_Banyak);
						gotoxy(1,30);cout<<"Alpa Predikat6 : "<<alpa6<<endl;
						Z6 = 24-(2*alpa6); //himpunan suhu optimal SEJUK
						gotoxy(1,31);cout<<"Nilai Z6 \t: "<<Z6;
					
						//7. Jika Luas Ruangan BESAR dan Jumlah Orang SEDIKIT Maka DINGIN
						alpa7 = min(NA_LuasRuangan_Besar,NA_JumlahOrang_Sedikit);
						gotoxy(1,33);cout<<"Alpa Predikat7 : "<<alpa7<<endl;
						Z7 = 20-(4*alpa7); //himpunan suhu optimal DINGIN
						gotoxy(1,34);cout<<"Nilai Z7 \t: "<<Z7;
						
						
						//8. Jika Luas Ruangan BESAR dan Jumlah Orang SEDANG Maka SEDANG
						alpa8 = min(NA_LuasRuangan_Besar,NA_JumlahOrang_Sedang);
						gotoxy(1,36);cout<<"Alpa Predikat8 : "<<alpa8<<endl;
						Z8 = 16-(4*alpa8); //himpunan suhu optimal SEDANG
						gotoxy(1,37);cout<<"Nilai Z8 \t: "<<Z8;
						
						//9. Jika Luas Ruangan BESAR dan Jumlah Orang BANYAK Maka SEJUK
						alpa9 = min(NA_LuasRuangan_Besar,NA_JumlahOrang_Banyak);
						gotoxy(1,39);cout<<"Alpa Predikat9 : "<<alpa9<<endl<<endl;
						Z9 = 24-(2*alpa9); //himpunan suhu optimal SEJUK
						gotoxy(1,40);cout<<"Nilai Z9 \t: "<<Z9;
					
						//Mencari Z Akhir dan Menentukan Dingin, Sedang, atau Sejuk
						getch();
						tempZ1 = (alpa1*Z1)+(alpa2*Z2)+(alpa3*Z3)+(alpa4*Z4)+(alpa5*Z5)+(alpa6*Z6)+(alpa7*Z7)+(alpa8*Z8)+(alpa9*Z9);
						tempZ2 = (float)alpa1+(float)alpa2+(float)alpa3+(float)alpa4+(float)alpa5+(float)alpa6+(float)alpa7+(float)alpa8+(float)alpa9;
						Z = (float)(tempZ1)/(float)(tempZ2);
						if(Z>=16 and Z<20){
							Suhu = "Dingin";
						} else if (Z>=20 and Z<24)
							{
									Suhu = "Sedang";
							} else if (Z>=24 and Z>26)
								{
										Suhu = "Sejuk";
								}
						gotoxy(25,18);cout<<"Z Akhir : "<<Z;								
						getch();
						gotoxy(25,20);cout<<"Jadi Suhu Maksimum yang Harus Di keluarkan";
						gotoxy(25,21);cout<<"AC Pada Ruangan Tersebut Adalah Sebesar : "<<Z<<" Celcius";
						gotoxy(25,22);cout<<"yang termasuk ke dalam ukuran suhu : "<<Suhu;
						} //Penutup kurung untuk animasi loading
						getch();
			break;
			
			case 2 :	gotoxy(20,9);cout<<"Terima Kasih :)";
						getch();
						break;
			
			default:	gotoxy(20,9);cout<<"Menu Tidak Tersedia!";
						getch();
		}
		system("cls");
	} while (Menu != 2);
	
	system("PAUSE");
	return 0;
}
