Pembahasan ini akan menjelaskan proses-proses yang terjadi dalam program yang telah diimplementasikan menggunakan Fuzzy logic.

Fuzzy logic adalah peningkatan dari logika Boolean yang berhadapan dengan konsep kebenaran sebagian. Saat logika klasik menyatakan bahwa segala hal dapat diekspresikan dalam istilah biner (0 atau 1, hitam atau putih, ya atau tidak), Fuzzy logic menggantikan kebenaran boolean dengan tingkat kebenaran.

Pada penerapannya, Fuzzy logic kadang di gunakan untuk menentukan cold, warm, dan hot pada suatu ruangan yang bersuhu telah diketahui. Awal mula program sudah diberi nilai awal untuk cold, warm, dan hot


    I. Proses Program
    1. Penilaian Awal Suatu Variabel
Data yang akan di cari mula-mula diberi nilai min, max dan mid.	


    2. Pemrosesan Nilai Input
Selanjutnya, user akan diminta untuk mengberikan nilai suatu suhu lalu program akan mengembalikan nilainya dalam bentuk cold, warm, dan hot.
